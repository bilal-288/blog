@extends('admin/master')
@section('admin')

<div class="content-body">
            <div class="container-fluid">
                <div class="row page-titles mx-0">
                    <div class="col-sm-6 p-md-0">
                        <div class="welcome-text">
                            <h4>Hi, welcome back!</h4>
                            <span class="ml-1">Datatable</span>
                        </div>
                    </div>
                    <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Table</a></li>
                            <li class="breadcrumb-item active"><a href="javascript:void(0)">Datatable</a></li>
                        </ol>
                    </div>
                </div>
                <!-- row -->


                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Basic Datatable</h4>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                <table id="example" class="display" style="min-width: 845px">
                                        <thead>
                                            <tr>
                                                <th>Sr#</th>
                                                <th>Title</th>
                                                <th>News</th>
                                                <th>Category ID</th>
                                                <th>Creator</th>
                                                <th>Edit</th>
                                                <th>Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <?php $i=1; ?>

                                            @foreach($products as $product)
                                            <tr id="bilal" class="bilal">
                                                <td><?php echo $i++; ?></td>
                                                <td>{{$product->title}}</td>
                                                <td>{{$product->news}}</td>
                                                <td>{{$product->cat_id}}</td>
                                                <td>{{$product->creator}}</td>
                                                 
                                            <td>
                                                    <a href="{{url('editpost/'.$product->id)}}">
                                                    <button class="btn-info" ><i class="fa fa-edit"></i></button>
                                                    </a>
                                            </td>

                                <td>
                                    
                                 <form action="{{url('deletepost/')}}" method="post" enctype='multipart/form-data'>
                                            
                                            @csrf

                                            <input type="hidden" name = "id" value="{{$product->id}}">
                                                    
                                            <button type = "submit" class="btn-danger">
                                                 <i class="fa fa-trash"></i>
                                            </button> 

                                                </form>   
                                               
                                                    
                          </td>

                                            </tr>
                                            @endforeach

                                           </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--**********************************
            Content body end
        ***********************************-->

        @endsection
