@extends('admin/master')
@section('admin')

<div class="content-body">
            <div class="container-fluid">
                <div class="row page-titles mx-0">
                    <div class="col-sm-6 p-md-0">
                        <div class="welcome-text">
                            <h4>Add Category Form</h4> 
                        </div>
                    </div>

                    <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Form</a>
                            </li>
                            <li class="breadcrumb-item active"><a href="javascript:void(0)">Validation</a></li>
                        </ol>
                    </div>

                </div>
                <!-- row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Update Category</h4>
                            </div>
                            <div class="card-body">
                                <div class="form-validation">

                                 <form class="form-valide" action="{{url('updatecategory/'.$edit_category[0]->id)}}" method="post" enctype='multipart/form-data'>
                                       
                                       @csrf
                                      
                                         <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label" for="val-username">Category Name
                                                        <span class="text-danger">*</span>
                                                    </label>
                                                    <div class="col-lg-6">
                                                        <input type="text" class="form-control" id="val-username" name="catname" 
                                                        value="<?php echo 
                                                        $edit_category[0]->name; ?>">
                                                    </div>
                                          </div>

                                                
                                             <div class="form-group row">
                                                    <div class="col-lg-8 ml-auto">
                                                        <button type="submit" class="btn btn-primary">Save Category</button>
                                             </div>
                                                
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
        @endsection
