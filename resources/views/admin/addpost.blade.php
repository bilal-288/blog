@extends('admin/master')
@section('admin')

<div class="content-body">
            <div class="container-fluid">
                <div class="row page-titles mx-0">
                    <div class="col-sm-6 p-md-0">
                        <div class="welcome-text">
                            <h4>Add product Form</h4>
                            <p class="mb-1">Validation</p>
                        </div>
                    </div>
                    <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Form</a>
                            </li>
                            <li class="breadcrumb-item active"><a href="javascript:void(0)">Validation</a></li>
                        </ol>
                    </div>
                </div>
                <!-- row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Form Validation</h4>
                            </div>
                            <div class="card-body">
                                <div class="form-validation">

                                 <form class="form-valide" action="{{url('savepost')}}" method="post" enctype='multipart/form-data'>
                                       @csrf
                                       
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label" for="val-username">Post Title
                                                        <span class="text-danger">*</span>
                                                    </label>
                                                    <div class="col-lg-6">
                                                        <input type="text" class="form-control" id="val-username" name="post_title" placeholder="Enter  Tilte" required>
                                                    </div>
                                                </div>

                                                
                                                

                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">News
                                                     <span
                                                            class="text-danger">*</span>
                                                    </label>

                                                    <div class="col-lg-6">
                                                        <textarea class="form-control" id="desc" name="news" rows="5" placeholder=
                                                        "News" required></textarea>
                                                    </div>

                                                </div>

                                                

                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label" >
                                                        Created User
                                                        <span class="text-danger">*</span>
                                                    </label>
                                                    
                                                    <div class="col-lg-6">
                                                        <input type="text" class="form-control" id="catid" name="created_user" placeholder="Enter the Creator name " required>
                                                    </div>
                                                </div>



                                               <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">
                                                        Post Category <span
                                                      class="text-danger">*</span>
                                                    </label>
                                                   <div class="col-lg-6">
                                                        <select class="form-control" id="catstatus" name="postcategory" style="width:40%;" >       
                                                        <option  selected="true" disabled="disabled">Choose product category</option> 
                                                            @foreach($listcategory as $category)
                                                            <option value="{{$category->id}}">
                                                                {{$category->name}}
                                                            </option>
                                                            @endforeach
                                                         </select>   
                                                    </div>
                                               </div>

                                                
                                            <div class="form-group row ">
                                               <!-- <div class="col-lg-4"></div> -->
                                                <div class="col-lg-6 offset-lg-4">
                                                    <button type="submit" class="btn btn-primary">Submit</button>
                                                </div>
                                            </div>

                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
        @endsection