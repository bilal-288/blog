<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Session;

class CategoryController extends Controller
{
    function showcategoryform()
    { 
    	return view('admin/addcategoryform');
    }

    function savecategory(Request $req)
    {
    	$data= 
    	[
    		'name'=>$req->catname,
    	];

    	 DB::table('categories')->insert($data);
      	return redirect('admin/dashboard');
    }

     function view()
    {
        return view('admin/dashboard');
    }

    function showcategories()
    {
    	$temp['all_categories'] = DB::table('categories')->get();
    	return view('admin/showcategory',$temp);
    } 

    function deletecategory(Request $req)
    {
        $id = $req->input('id');
        
        DB::table('categories')->where('id',$id)->delete();

         //in redirect we use route
        return redirect('categories');
    }


    function editcategory($id)
    {
        $data['edit_category'] = DB::table('categories')->where('id',$id)->get();

        
        return view('admin/editcategory', $data);
    }

     function updatecategory(Request $request, $id)
    {

        $category = [
     
       'name' => $request->catname,

      ];


      DB::table('categories')->where('id',$id)->update($category);
        return redirect('categories'); 
    }
}
