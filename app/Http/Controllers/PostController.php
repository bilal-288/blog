<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use DB;

class PostController extends Controller
{
    function addpostform()
    {
        
        $data['listcategory'] = DB::table('categories')->get();

        return view('admin/addpost',$data);
    }



    function savepost(Request $request)
    {
     

      $post = 
      [
     
       'title' => $request->post_title,
       'news'=> $request->news,
        'creator' => $request->created_user,
           'cat_id' => $request->postcategory,
      ];


      DB::table('posts')->insert($post);
      return redirect('admin/dashboard');
            
    }


    function showpost()
    {
    	//get all the records from  the  table
    	$data['products'] = DB::table('posts')->get();
    	return view('admin/showpost',$data);

    }

    function deletepost(Request $request)
    {
        $id = $request->input('id');
    	 DB::table('posts')->where('id',$id)->delete();

         //in redirect we use route
    	return redirect('showpost');
    }


     function editpost($id)
    {
    	$data['ddd'] = DB::table('posts')->where('id',$id)->get(); 
    	return view('admin/editpost', $data);
    }

    function updatepost(Request $request,$id)
    {
    	
        //$id=$request->input('id');
    
        

        $data = 
        [
    		'title'=>$request->input('post_title'),
    		'news'=>$request->input('news'),
    		'creator'=>$request->input('created_user'),
    	];

       /* echo $data['stack'];
        die();*/
    	DB::table('posts')->where('id',$id)->update($data);
    	return redirect('showpost'); 


    }


}
