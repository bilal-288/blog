<?php

use App\Http\Controllers\blog;
use App\Http\Controllers\CategoryController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('admin/dashboard','blog@index');
Route::get('categoryform','CategoryController@showcategoryform');
Route::post('savecategory', 'CategoryController@savecategory');
Route::get('dashboard', 'CategoryController@view');
Route::get('categories', 'CategoryController@showcategories');
Route::post('deletecategory/', 'CategoryController@deletecategory');
Route::get('editcategory/{id}', 'CategoryController@editcategory');
Route::post('updatecategory/{id}', 'CategoryController@updatecategory');

Route::get('postform','PostController@addpostform');
Route::post('savepost','PostController@savepost');
Route::get('showpost','PostController@showpost');
Route::post('deletepost/','PostController@deletepost');
Route::get('editpost/{id}', 'PostController@editpost');
Route::post('updatepost/{id}', 'PostController@updatepost');